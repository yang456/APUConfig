/**
  ******************************************************************************
  * @author  yang feng wu 
  * @version V1.0.0
  * @date    2019/10/12
  * @brief   
  ******************************************************************************
	长按 PB5 大约3S 指示灯(PC13)快闪 进入配网状态
	PB2 连接 Wi-Fi RST引脚
  ******************************************************************************
  */
#include "include.h"

//缓存数据使用
char MainBuffer[MainBufferLen];//缓存数据,全局通用
u32  MainLen=0;      //全局通用变量
char *MainString;    //全局通用变量

u32 RendTHCnt = 0;//定时采集温湿度数据

int main(void)
{
  NVIC_Configuration();
	uart_init(115200);	 //串口初始化为115200
	GpioInit();
	DelayInit();
//	rbCreate(&Uart1rb,Usart1SendBuff,Usart1SendLen);//创建环形队列--串口1
	
	Timer2_Config();
	
	IWDG_Init(IWDG_Prescaler_256,156*5);
	
	OLED_Init();			
	OLED_Clear()  	; 
	
	OLED_ShowCHinese(36,0,0);//温
	OLED_ShowCHinese(54,0,1);//湿
  OLED_ShowCHinese(72,0,2);//度
	
	OLED_ShowString(25,3,"T :",16);
	OLED_ShowString(25,5,"H :",16); 
  OLED_ShowCHinese(80,3,3);//℃
	OLED_ShowString(80,5," %",16); 
	
	while(1)
	{
		IWDG_Feed();//喂狗
		
		SmartConfigKey();
		if(KeySmartConfig[3] == 1)
		{
			if(KeySmartConfig[5]>=3000)//按下时间大于3S
			{
				KeySmartConfig[3] = 0;//清零以后,只有按键松开再按下的时候才会进入
				if(!SmartConfigFlage)
				{
					SmartConfigFlage = 1;
					if(APUConfig())//配网成功
					{
						
					}
				}
			}
		}
	
		//定时采集温湿度数据
		if(RendTHCnt>=5000)
		{
			DHT11_Read_Data();
			RendTHCnt=0;
			OLED_ShowNum(55,3, DHT11Data[0],2,16);//
			OLED_ShowNum(55,5, DHT11Data[2],2,16);//
	  }
	}
}